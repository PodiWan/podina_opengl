#pragma once
#include "glm/glm/vec3.hpp"

class BoundingBox
{
public:
	glm::vec3 min;
	glm::vec3 max;

	BoundingBox() : min(2147483648.0f), max(-2147483648.0f) {}
};
