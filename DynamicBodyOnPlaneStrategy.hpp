#pragma once

#include "Strategy.hpp"
#include "glm/glm/vec3.hpp"

class DynamicBodyOnPlaneStrategy : public Strategy
{
public:
	DynamicBodyOnPlaneStrategy();
	~DynamicBodyOnPlaneStrategy() override;
	void ComputeMotion(float delta, glm::vec3& position, glm::vec3& velocity) override;
};