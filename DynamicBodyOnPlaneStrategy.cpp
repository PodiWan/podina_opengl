#include "DynamicBodyOnPlaneStrategy.hpp"

DynamicBodyOnPlaneStrategy::DynamicBodyOnPlaneStrategy() = default;
// TODO: handle destructor
DynamicBodyOnPlaneStrategy::~DynamicBodyOnPlaneStrategy() = default;
void DynamicBodyOnPlaneStrategy::ComputeMotion(const float delta, glm::vec3 & position, glm::vec3 & velocity)
{
	position += velocity * delta;
}