#pragma once
#include "Strategy.hpp"

class Context
{
private:
	Strategy* strategy_;

public:
	Context(Strategy *strategy = nullptr) : strategy_(strategy) {}

	~Context();
	void ComputeMotion(float delta, glm::vec3& position, glm::vec3& velocity) const;
};
