#include "Camera.hpp"

namespace gps {

    //Camera constructor
    Camera::Camera(glm::vec3 cameraPosition, glm::vec3 cameraTarget, glm::vec3 cameraUp)
    {
        this->cameraPosition = cameraPosition;
        this->cameraTarget = cameraTarget;
        this->cameraUpDirection = cameraUp;
        this->cameraFrontDirection = glm::normalize(cameraTarget - cameraPosition);
        this->cameraRightDirection = glm::normalize(glm::cross(this->cameraFrontDirection, this->cameraUpDirection));
    }

    //return the view matrix, using the glm::lookAt() function
    glm::mat4 Camera::getViewMatrix()
	{
        return glm::lookAt(cameraPosition, cameraPosition + cameraFrontDirection, cameraUpDirection);
    }

    float Camera::isCameraInsideBoundingBox(MOVE_DIRECTION direction, float speed)
    {
        glm::vec3 point = getCameraNewPosition(direction, speed);
        for (auto model : boundingBoxes) {
            if ((point.x >= model.second->GetBoundingBox().min.x && point.x <= model.second->GetBoundingBox().max.x) &&
                (point.y >= model.second->GetBoundingBox().min.y && point.y <= model.second->GetBoundingBox().max.y) &&
                (point.z >= model.second->GetBoundingBox().min.z && point.z <= model.second->GetBoundingBox().max.z))
                return true;
        }
        return false;
    }

    glm::vec3 Camera::getCameraNewPosition(MOVE_DIRECTION direction, float speed) {
        switch (direction) {
        case MOVE_FORWARD:
            return cameraPosition + cameraFrontDirection * speed;

        case MOVE_BACKWARD:
            return cameraPosition - cameraFrontDirection * speed;

        case MOVE_RIGHT:
            return cameraPosition + cameraRightDirection * speed;

        case MOVE_LEFT:
            return cameraPosition - cameraRightDirection * speed;
        }
    }

    glm::vec3 Camera::getCameraTarget()
    {
        return cameraTarget;
    }

    glm::vec3 Camera::getPosition()
    {
        return cameraPosition;
    }

    void Camera::setPosition(glm::vec3 position)
    {
        this->cameraPosition = position;
    }

    glm::vec3 Camera::getDirection()
    {
        return cameraFrontDirection;
    }

    //update the camera internal parameters following a camera move event
    void Camera::move(MOVE_DIRECTION direction, float speed)
    {
        if (!isCameraInsideBoundingBox(direction, speed)) {
            cameraPosition = getCameraNewPosition(direction, speed);
        }
    }

    //update the camera internal parameters following a camera rotate event
    //yaw - camera rotation around the y axis
    //pitch - camera rotation around the x axis
    void Camera::rotate(float pitch, float yaw)
	{
        this->cameraFrontDirection = glm::vec3(
            cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
            sin(glm::radians(pitch)),
            sin(glm::radians(yaw)) * cos(glm::radians(pitch))
        );

        this->cameraRightDirection = glm::normalize(glm::cross(cameraFrontDirection, cameraUpDirection));
    }
}