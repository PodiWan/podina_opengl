#pragma once

#include "Strategy.hpp"
#include "glm/glm/vec3.hpp"

class DynamicBodyStrategy : public Strategy
{
public:
	DynamicBodyStrategy();
	~DynamicBodyStrategy() override;
	void ComputeMotion(float delta, glm::vec3& position, glm::vec3& velocity) override;
};