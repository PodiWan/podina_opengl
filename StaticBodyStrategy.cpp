#include "StaticBodyStrategy.hpp"

StaticBodyStrategy::StaticBodyStrategy() = default;
// TODO: handle destructor
StaticBodyStrategy::~StaticBodyStrategy() = default;
void StaticBodyStrategy::ComputeMotion(const float delta, glm::vec3& position, glm::vec3& velocity) {}