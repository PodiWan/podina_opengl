#pragma once
#include <GL/glew.h>

#include "BoundingBox.hpp"
#include "Context.hpp"
#include "DynamicBodyOnPlaneStrategy.hpp"
#include "DynamicBodyStrategy.hpp"
#include "Model3D.hpp"
#include "nameof.hpp"
#include "glm/glm/gtc/matrix_inverse.hpp"
#include "glm/glm/gtc/type_ptr.hpp"
#include "ModelType.hpp"
#include "StaticBodyStrategy.hpp"

class Model
{

private:
    glm::mat4 model;
    glm::mat4 modelBoundingBox;
    glm::mat3 normalMatrix;
    GLint modelLoc;
    GLint boxModelLoc;
    GLint normalMatrixLoc;
    glm::vec3 position;
    gps::Model3D model3d;
    gps::Shader shader;
    float mass;
    glm::vec3 scale;
    glm::vec3 velocity;
    glm::vec3 acceleration;
    Context* context_;

    void CreateBoundingBox()
    {
        auto modelMeshes = model3d.getMeshes();

        for (auto modelMesh : modelMeshes)
        {
            for (gps::Vertex vertex : modelMesh.vertices)
            {
                if (vertex.Position.x < this->boundingBox_.min.x)
                {
                    this->boundingBox_.min.x = vertex.Position.x;
                }
                else if (vertex.Position.x > this->boundingBox_.max.x)
                {
                    this->boundingBox_.max.x = vertex.Position.x;
                }

                if (vertex.Position.y < this->boundingBox_.min.y)
                {
                    this->boundingBox_.min.y = vertex.Position.y;
                }
                else if (vertex.Position.y > this->boundingBox_.max.y)
                {
                    this->boundingBox_.max.y = vertex.Position.y;
                }

                if (vertex.Position.z < this->boundingBox_.min.z)
                {
                    this->boundingBox_.min.z = vertex.Position.z;
                }
                else if (vertex.Position.z > this->boundingBox_.max.z)
                {
                    this->boundingBox_.max.z = vertex.Position.z;
                }
            }
        }
        this->boundingBox_.min -= this->position;
        this->boundingBox_.max -= this->position;

        this->boundingBox_.min *= this->scale;
        this->boundingBox_.max *= this->scale;
    }

public:
    gps::Model3D boundingBoxObject_;
    BoundingBox boundingBox_;
    float rotation;

    Model(glm::vec3 position, ModelType modelType) : modelLoc(GLint()), normalMatrixLoc(GLint()), position(position)
    {
        this->model = glm::mat4();
        this->rotation = 1.0f;
        this->normalMatrix = glm::mat3();
        this->shader = gps::Shader();
        this->velocity = glm::vec3();
        this->boundingBox_ = BoundingBox();
        this->SetModelType(modelType);
        this->boundingBoxObject_ = gps::Model3D();
        this->scale = glm::vec3(1.0f);
    }

    Model(ModelType modelType) : Model(glm::vec3(0, 0, 0), modelType) {}

    void SetModelType(ModelType modelType)
    {
        switch(modelType)
        {
        case StaticBody: {this->context_ = new Context(new StaticBodyStrategy); break; }
        case DynamicBody: {this->context_ = new Context(new DynamicBodyStrategy); break; }
        case DynamicBodyOnPlane: {this->context_ = new Context(new DynamicBodyOnPlaneStrategy); break; }
        }
    }

    void Draw(bool drawWireframe = false)
    {
        //send model matrix data to shader
        glUniformMatrix4fv(this->modelLoc, 1, GL_FALSE, glm::value_ptr(this->model));
        this->model3d.Draw(this->shader, false);
        glUniformMatrix4fv(this->boxModelLoc, 1, GL_FALSE, glm::value_ptr(this->modelBoundingBox));
        if (drawWireframe)
            this->boundingBoxObject_.Draw(this->shader, true);
    }

    void LoadModel(const std::string& path)
    {
        this->model3d.LoadModel(path);
        this->CreateBoundingBox();
    }

    void LoadShader(std::string vertexShaderFileName, std::string fragmentShaderFileName)
    {
        this->shader.loadShader(vertexShaderFileName, fragmentShaderFileName);
    }

    void UseShaderProgram()
    {
        this->shader.useShaderProgram();
    }

    void SetShader(gps::Shader shader)
    {
        this->shader = shader;
    }


    void SetPosition(glm::vec3 position)
    {
        this->position = position;
    }

    glm::vec3 GetPosition()
    {
        return this->position;
    }

    BoundingBox GetBoundingBox()
    {
        BoundingBox actualbox;
        actualbox.min = 0.6666f * (this->boundingBox_.min + this->position);
        actualbox.max = 0.6666f * (this->boundingBox_.max + this->position);
        return actualbox;
    }

    void SetScale(glm::vec3 scale)
    {
        this->scale = scale;
    }

    glm::vec3 GetVelocity()
    {
        return this->velocity;
    }

    glm::vec3 GetSize()
    {
        return this->scale * glm::vec3(this->GetBoundingBox().max.x - this->GetBoundingBox().min.x,
            this->GetBoundingBox().max.y - this->GetBoundingBox().min.y,
            this->GetBoundingBox().max.z - this->GetBoundingBox().min.z);
    }

    void CreateModelMatrix(float angle, glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f))
    {
        model = glm::translate(glm::mat4(1.0f), this->position);
        model = glm::rotate(this->model, glm::radians(angle + this->rotation), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(this->model, this->scale * scale);
        modelLoc = glGetUniformLocation(this->shader.shaderProgram, nameof(model));

        this->velocity = glm::vec3(glm::vec4(this->velocity, 1.0f) * glm::rotate(glm::mat4(1.0f),
            glm::radians(angle * this->rotation), glm::vec3(0.0f, 1.0f, 0.0f)));

        modelBoundingBox = glm::translate(glm::mat4(1.0f), this->position);
        modelBoundingBox = glm::rotate(modelBoundingBox, glm::radians(angle + this->rotation), glm::vec3(0.0f, 1.0f, 0.0f));
        modelBoundingBox = glm::scale(modelBoundingBox, this->scale * this->GetSize());
        boxModelLoc = glGetUniformLocation(this->shader.shaderProgram, nameof(model));
    }

    void ComputeNormalMatrix(glm::mat4 view)
    {
        normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
        normalMatrixLoc = glGetUniformLocation(this->shader.shaderProgram, nameof(normalMatrix));
    }

    void SetVelocity(glm::vec3 velocity)
    {
        this->velocity = velocity;
    }

    void ComputeMotion(float delta)
    {
        //send model normal matrix data to shader
        // glUniformMatrix3fv(this->normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(this->normalMatrix));
        this->context_->ComputeMotion(delta, this->position, this->velocity);
    }
};