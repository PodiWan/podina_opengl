#ifndef Camera_hpp
#define Camera_hpp

#include <map>

#include "glm/glm/glm.hpp"
#include "glm/glm/gtx/transform.hpp"

#include <string>

#include "Model.hpp"

namespace gps {
    
    enum MOVE_DIRECTION {MOVE_FORWARD, MOVE_BACKWARD, MOVE_RIGHT, MOVE_LEFT};
    
    class Camera
    {
    public:
        //Camera constructor
        Camera(glm::vec3 cameraPosition, glm::vec3 cameraTarget, glm::vec3 cameraUp);
        //return the view matrix, using the glm::lookAt() function
        glm::mat4 getViewMatrix();
        //update the camera internal parameters following a camera move event
        void move(MOVE_DIRECTION direction, float speed);
        //update the camera internal parameters following a camera rotate event
        //yaw - camera rotation around the y axis
        //pitch - camera rotation around the x axis
        void rotate(float pitch, float yaw);
        std::map<std::string, Model*> boundingBoxes = {};
        glm::vec3 getPosition();
        void setPosition(glm::vec3 position);
        glm::vec3 getDirection();
        glm::vec3 getCameraTarget();
        
    private:
        glm::vec3 cameraPosition;
        glm::vec3 cameraTarget;
        glm::vec3 cameraFrontDirection;
        glm::vec3 cameraRightDirection;
        glm::vec3 cameraUpDirection;
        float isCameraInsideBoundingBox(MOVE_DIRECTION direction, float speed);
        glm::vec3 getCameraNewPosition(MOVE_DIRECTION direction, float speed);
    };
    
}

#endif /* Camera_hpp */
