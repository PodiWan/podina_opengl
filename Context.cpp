#include "Context.hpp"

Context::~Context()
{
	delete this->strategy_;
}

void Context::ComputeMotion(float delta, glm::vec3& position, glm::vec3& velocity) const
{
	this->strategy_->ComputeMotion(delta, position, velocity);
}