#pragma once

#include "Strategy.hpp"
#include "glm/glm/vec3.hpp"

class StaticBodyStrategy : public Strategy
{
public:
	StaticBodyStrategy();
	~StaticBodyStrategy() override;
	void ComputeMotion(float delta, glm::vec3& position, glm::vec3& velocity) override;
};